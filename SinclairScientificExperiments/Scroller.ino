#include "GPIO.h"

//
// 0 a g d b c f e
// 0 1 1 1 1 1 1 1 // digit
// 1 1 1 1 1 1 1 0 // shift left
// 0 1 1 0 1 0 1 0 // and with mask
// 0 1 1 0 1 0 1 0 // digit scrolled one step up
//
// 0 a g d b c f e
// 0 1 1 1 1 1 1 1 // digit
// 0 0 1 1 1 1 1 1 // shift right
// 0 0 1 1 0 1 0 1 // and with mask
// 0 0 1 1 0 1 0 1 // digit scrolled one step dn
//

const byte DIGITS[] PROGMEM = {
  B1011111,
  B0001100,
  B1111001,
  B1111100,
  B0101110,
  B1110110,
  B1110111,
  B1001100,
  B1111111,
  B1111110
};

GPIO<BOARD::D5>  SegmentA;
GPIO<BOARD::D8>  SegmentB;
GPIO<BOARD::D10> SegmentC;
GPIO<BOARD::D19> SegmentD;
GPIO<BOARD::D9>  SegmentE;
GPIO<BOARD::D6>  SegmentF;
GPIO<BOARD::D14> SegmentG;
GPIO<BOARD::D18> SegmentDP;

GPIO<BOARD::D2>  Digit1;
GPIO<BOARD::D3>  Digit2;
GPIO<BOARD::D4>  Digit3;
GPIO<BOARD::D17> Digit4;
GPIO<BOARD::D16> Digit5;
GPIO<BOARD::D7>  Digit6;
GPIO<BOARD::D15> Digit7;
GPIO<BOARD::D11> Digit8;
GPIO<BOARD::D12> Digit9;

#define SegmentOn low
#define SegmentOff high
#define DigitOn high
#define DigitOff low
#define IsKeyPushed > 100

void allSegmentOutput() {
  SegmentA.output();
  SegmentB.output();
  SegmentC.output();
  SegmentD.output();
  SegmentE.output();
  SegmentF.output();
  SegmentG.output();
  SegmentDP.output();
}

void allSegmentInput() {
  SegmentA.input();
  SegmentB.input();
  SegmentC.input();
  SegmentD.input();
  SegmentE.input();
  SegmentF.input();
  SegmentG.input();
  SegmentDP.input();
}

void allDigitOutput() {
  Digit1.output();
  Digit2.output();
  Digit3.output();
  Digit4.output();
  Digit5.output();
  Digit6.output();
  Digit7.output();
  Digit8.output();
  Digit9.output();
}

void allDigitInput() {
  Digit1.input();
  Digit2.input();
  Digit3.input();
  Digit4.input();
  Digit5.input();
  Digit6.input();
  Digit7.input();
  Digit8.input();
  Digit9.input();
}

void allSegmentOff() {
  SegmentA.SegmentOff();
  SegmentB.SegmentOff();
  SegmentC.SegmentOff();
  SegmentD.SegmentOff();
  SegmentE.SegmentOff();
  SegmentF.SegmentOff();
  SegmentG.SegmentOff();
  SegmentDP.SegmentOff();
}

void allDigitOff() {
  Digit1.DigitOff();
  Digit2.DigitOff();
  Digit3.DigitOff();
  Digit4.DigitOff();
  Digit5.DigitOff();
  Digit6.DigitOff();
  Digit7.DigitOff();
  Digit8.DigitOff();
  Digit9.DigitOff();
}

byte scrollup(byte d, byte times)
{
  byte raw = pgm_read_byte(DIGITS + d);

  for (byte i = 0; i < times; i++)
  {
    raw = raw << 1;
    raw = raw & B01101010;
  }

  return raw;
}

byte scrolldn(byte d, byte times)
{
  byte raw = pgm_read_byte(DIGITS + d);

  for (byte i = 0; i < times; i++)
  {
    raw = raw >> 1;
    raw = raw & B00110101;
  }

  return raw;
}

void display(byte raw)
{
  if (raw & B01000000)
  {
    SegmentA.SegmentOn();
  }
  else
  {
    SegmentA.SegmentOff();
  }

  if (raw & B00100000)
  {
    SegmentG.SegmentOn();
  }
  else
  {
    SegmentG.SegmentOff();
  }

  if (raw & B00010000)
  {
    SegmentD.SegmentOn();
  }
  else
  {
    SegmentD.SegmentOff();
  }

  if (raw & B00001000)
  {
    SegmentB.SegmentOn();
  }
  else
  {
    SegmentB.SegmentOff();
  }

  if (raw & B00000100)
  {
    SegmentC.SegmentOn();
  }
  else
  {
    SegmentC.SegmentOff();
  }

  if (raw & B00000010)
  {
    SegmentF.SegmentOn();
  }
  else
  {
    SegmentF.SegmentOff();
  }

  if (raw & B00000001)
  {
    SegmentE.SegmentOn();
  }
  else
  {
    SegmentE.SegmentOff();
  }

  Digit1.DigitOn();

}

void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);

  allSegmentOff();
  allDigitOff();
  allSegmentOutput();
  allDigitOutput();

  /*
    for (byte i = 0; i < 10; i++)
    {
    Serial.println(pgm_read_byte(DIGITS + i));
    }

    for (byte i = 0; i < 4; i++)
    {
    Serial.println(F("scrollup"));
    Serial.println(scrollup(8, i));
    }

    for (byte i = 0; i < 4; i++)
    {
    Serial.println(F("scrolldn"));
    Serial.println(scrolldn(8, i));
    }
  */

}

void loop() {
  // put your main code here, to run repeatedly:

  for (byte j = 0; j < 9; j++)
  {
    for (byte i = 0; i < 5; i++)
    {
      display(scrollup(j, i) | scrolldn(j + 1, 4 - i));
      delay(150);
    }
  }

  for (byte j = 9; j > 0; j--)
  {
    for (byte i = 0; i < 5; i++)
    {
      display(scrollup(j - 1, 4 - i) | scrolldn(j, i));
      delay(150);
    }
  }

}